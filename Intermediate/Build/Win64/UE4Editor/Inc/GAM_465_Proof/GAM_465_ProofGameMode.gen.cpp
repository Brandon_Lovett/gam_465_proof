// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "GAM_465_ProofGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGAM_465_ProofGameMode() {}
// Cross Module References
	GAM_465_PROOF_API UClass* Z_Construct_UClass_AGAM_465_ProofGameMode_NoRegister();
	GAM_465_PROOF_API UClass* Z_Construct_UClass_AGAM_465_ProofGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_GAM_465_Proof();
// End Cross Module References
	void AGAM_465_ProofGameMode::StaticRegisterNativesAGAM_465_ProofGameMode()
	{
	}
	UClass* Z_Construct_UClass_AGAM_465_ProofGameMode_NoRegister()
	{
		return AGAM_465_ProofGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AGAM_465_ProofGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_GAM_465_Proof,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "GAM_465_ProofGameMode.h" },
				{ "ModuleRelativePath", "GAM_465_ProofGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AGAM_465_ProofGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AGAM_465_ProofGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGAM_465_ProofGameMode, 2372830091);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGAM_465_ProofGameMode(Z_Construct_UClass_AGAM_465_ProofGameMode, &AGAM_465_ProofGameMode::StaticClass, TEXT("/Script/GAM_465_Proof"), TEXT("AGAM_465_ProofGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGAM_465_ProofGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
