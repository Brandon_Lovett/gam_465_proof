// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM_465_PROOF_Spawner_generated_h
#error "Spawner.generated.h already included, missing '#pragma once' in Spawner.h"
#endif
#define GAM_465_PROOF_Spawner_generated_h

#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execStopTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->StopTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ResetTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFireLoop) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->FireLoop(); \
		P_NATIVE_END; \
	}


#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execStopTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->StopTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ResetTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFireLoop) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->FireLoop(); \
		P_NATIVE_END; \
	}


#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_ASpawner(); \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_465_Proof"), NO_API) \
	DECLARE_SERIALIZER(ASpawner) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_ASpawner(); \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_465_Proof"), NO_API) \
	DECLARE_SERIALIZER(ASpawner) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public:


#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawner)


#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_PRIVATE_PROPERTY_OFFSET
#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_13_PROLOG
#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_RPC_WRAPPERS \
	gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_INCLASS \
	gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_INCLASS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_Spawner_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_465_proof_Source_GAM_465_Proof_Spawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
