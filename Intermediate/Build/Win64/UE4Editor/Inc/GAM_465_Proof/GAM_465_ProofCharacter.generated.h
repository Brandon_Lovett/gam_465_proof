// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM_465_PROOF_GAM_465_ProofCharacter_generated_h
#error "GAM_465_ProofCharacter.generated.h already included, missing '#pragma once' in GAM_465_ProofCharacter.h"
#endif
#define GAM_465_PROOF_GAM_465_ProofCharacter_generated_h

#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPauseGame) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PauseGame(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUseKey) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->UseKey(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPickUpKey) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PickUpKey(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerDeath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerDeath(); \
		P_NATIVE_END; \
	}


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPauseGame) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PauseGame(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUseKey) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->UseKey(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPickUpKey) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PickUpKey(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerDeath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerDeath(); \
		P_NATIVE_END; \
	}


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAM_465_ProofCharacter(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_AGAM_465_ProofCharacter(); \
public: \
	DECLARE_CLASS(AGAM_465_ProofCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_465_Proof"), NO_API) \
	DECLARE_SERIALIZER(AGAM_465_ProofCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAGAM_465_ProofCharacter(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_AGAM_465_ProofCharacter(); \
public: \
	DECLARE_CLASS(AGAM_465_ProofCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_465_Proof"), NO_API) \
	DECLARE_SERIALIZER(AGAM_465_ProofCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAM_465_ProofCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAM_465_ProofCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM_465_ProofCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM_465_ProofCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM_465_ProofCharacter(AGAM_465_ProofCharacter&&); \
	NO_API AGAM_465_ProofCharacter(const AGAM_465_ProofCharacter&); \
public:


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM_465_ProofCharacter(AGAM_465_ProofCharacter&&); \
	NO_API AGAM_465_ProofCharacter(const AGAM_465_ProofCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM_465_ProofCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM_465_ProofCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGAM_465_ProofCharacter)


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SideViewCameraComponent() { return STRUCT_OFFSET(AGAM_465_ProofCharacter, SideViewCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AGAM_465_ProofCharacter, CameraBoom); }


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_10_PROLOG
#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_RPC_WRAPPERS \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_INCLASS \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_INCLASS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
