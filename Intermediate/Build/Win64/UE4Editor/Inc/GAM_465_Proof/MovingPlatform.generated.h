// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM_465_PROOF_MovingPlatform_generated_h
#error "MovingPlatform.generated.h already included, missing '#pragma once' in MovingPlatform.h"
#endif
#define GAM_465_PROOF_MovingPlatform_generated_h

#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_RPC_WRAPPERS
#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMovingPlatform(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_AMovingPlatform(); \
public: \
	DECLARE_CLASS(AMovingPlatform, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_465_Proof"), NO_API) \
	DECLARE_SERIALIZER(AMovingPlatform) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMovingPlatform(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_AMovingPlatform(); \
public: \
	DECLARE_CLASS(AMovingPlatform, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_465_Proof"), NO_API) \
	DECLARE_SERIALIZER(AMovingPlatform) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMovingPlatform(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMovingPlatform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMovingPlatform); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovingPlatform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMovingPlatform(AMovingPlatform&&); \
	NO_API AMovingPlatform(const AMovingPlatform&); \
public:


#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMovingPlatform(AMovingPlatform&&); \
	NO_API AMovingPlatform(const AMovingPlatform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMovingPlatform); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovingPlatform); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMovingPlatform)


#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_PRIVATE_PROPERTY_OFFSET
#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_11_PROLOG
#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_RPC_WRAPPERS \
	gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_INCLASS \
	gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_INCLASS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_465_proof_Source_GAM_465_Proof_MovingPlatform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
