// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "MovingPlatform.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovingPlatform() {}
// Cross Module References
	GAM_465_PROOF_API UClass* Z_Construct_UClass_AMovingPlatform_NoRegister();
	GAM_465_PROOF_API UClass* Z_Construct_UClass_AMovingPlatform();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_GAM_465_Proof();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AMovingPlatform::StaticRegisterNativesAMovingPlatform()
	{
	}
	UClass* Z_Construct_UClass_AMovingPlatform_NoRegister()
	{
		return AMovingPlatform::StaticClass();
	}
	UClass* Z_Construct_UClass_AMovingPlatform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_GAM_465_Proof,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "MovingPlatform.h" },
				{ "ModuleRelativePath", "MovingPlatform.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_speed_MetaData[] = {
				{ "Category", "ActorSpeed" },
				{ "ModuleRelativePath", "MovingPlatform.h" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_speed = { UE4CodeGen_Private::EPropertyClass::Float, "speed", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(AMovingPlatform, speed), METADATA_PARAMS(NewProp_speed_MetaData, ARRAY_COUNT(NewProp_speed_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_min_MetaData[] = {
				{ "Category", "ActorMinHeight" },
				{ "ModuleRelativePath", "MovingPlatform.h" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_min = { UE4CodeGen_Private::EPropertyClass::Float, "min", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(AMovingPlatform, min), METADATA_PARAMS(NewProp_min_MetaData, ARRAY_COUNT(NewProp_min_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_max_MetaData[] = {
				{ "Category", "ActorMaxHeight" },
				{ "ModuleRelativePath", "MovingPlatform.h" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_max = { UE4CodeGen_Private::EPropertyClass::Float, "max", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(AMovingPlatform, max), METADATA_PARAMS(NewProp_max_MetaData, ARRAY_COUNT(NewProp_max_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_direction_MetaData[] = {
				{ "Category", "ActorDirection" },
				{ "ModuleRelativePath", "MovingPlatform.h" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_direction = { UE4CodeGen_Private::EPropertyClass::Int, "direction", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(AMovingPlatform, direction), METADATA_PARAMS(NewProp_direction_MetaData, ARRAY_COUNT(NewProp_direction_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlatformMesh_MetaData[] = {
				{ "Category", "MovingPlatform" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "MovingPlatform.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlatformMesh = { UE4CodeGen_Private::EPropertyClass::Object, "PlatformMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00100000000a0009, 1, nullptr, STRUCT_OFFSET(AMovingPlatform, PlatformMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_PlatformMesh_MetaData, ARRAY_COUNT(NewProp_PlatformMesh_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_speed,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_min,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_max,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_direction,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_PlatformMesh,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AMovingPlatform>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AMovingPlatform::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMovingPlatform, 4181314885);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMovingPlatform(Z_Construct_UClass_AMovingPlatform, &AMovingPlatform::StaticClass, TEXT("/Script/GAM_465_Proof"), TEXT("AMovingPlatform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMovingPlatform);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
