// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM_465_PROOF_GAM_465_ProofGameMode_generated_h
#error "GAM_465_ProofGameMode.generated.h already included, missing '#pragma once' in GAM_465_ProofGameMode.h"
#endif
#define GAM_465_PROOF_GAM_465_ProofGameMode_generated_h

#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_RPC_WRAPPERS
#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAM_465_ProofGameMode(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_AGAM_465_ProofGameMode(); \
public: \
	DECLARE_CLASS(AGAM_465_ProofGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/GAM_465_Proof"), GAM_465_PROOF_API) \
	DECLARE_SERIALIZER(AGAM_465_ProofGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAGAM_465_ProofGameMode(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_AGAM_465_ProofGameMode(); \
public: \
	DECLARE_CLASS(AGAM_465_ProofGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/GAM_465_Proof"), GAM_465_PROOF_API) \
	DECLARE_SERIALIZER(AGAM_465_ProofGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAM_465_PROOF_API AGAM_465_ProofGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAM_465_ProofGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAM_465_PROOF_API, AGAM_465_ProofGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM_465_ProofGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAM_465_PROOF_API AGAM_465_ProofGameMode(AGAM_465_ProofGameMode&&); \
	GAM_465_PROOF_API AGAM_465_ProofGameMode(const AGAM_465_ProofGameMode&); \
public:


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAM_465_PROOF_API AGAM_465_ProofGameMode(AGAM_465_ProofGameMode&&); \
	GAM_465_PROOF_API AGAM_465_ProofGameMode(const AGAM_465_ProofGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAM_465_PROOF_API, AGAM_465_ProofGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM_465_ProofGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGAM_465_ProofGameMode)


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_PRIVATE_PROPERTY_OFFSET
#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_10_PROLOG
#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_RPC_WRAPPERS \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_INCLASS \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_INCLASS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_465_proof_Source_GAM_465_Proof_GAM_465_ProofGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
