// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GAM_465_PROOF_UnlockableDoor_generated_h
#error "UnlockableDoor.generated.h already included, missing '#pragma once' in UnlockableDoor.h"
#endif
#define GAM_465_PROOF_UnlockableDoor_generated_h

#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnOverlapBegin(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnOverlapBegin(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnlockableDoor(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_AUnlockableDoor(); \
public: \
	DECLARE_CLASS(AUnlockableDoor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_465_Proof"), NO_API) \
	DECLARE_SERIALIZER(AUnlockableDoor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAUnlockableDoor(); \
	friend GAM_465_PROOF_API class UClass* Z_Construct_UClass_AUnlockableDoor(); \
public: \
	DECLARE_CLASS(AUnlockableDoor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_465_Proof"), NO_API) \
	DECLARE_SERIALIZER(AUnlockableDoor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnlockableDoor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnlockableDoor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnlockableDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnlockableDoor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnlockableDoor(AUnlockableDoor&&); \
	NO_API AUnlockableDoor(const AUnlockableDoor&); \
public:


#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnlockableDoor(AUnlockableDoor&&); \
	NO_API AUnlockableDoor(const AUnlockableDoor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnlockableDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnlockableDoor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AUnlockableDoor)


#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_PRIVATE_PROPERTY_OFFSET
#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_14_PROLOG
#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_RPC_WRAPPERS \
	gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_INCLASS \
	gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_PRIVATE_PROPERTY_OFFSET \
	gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_INCLASS_NO_PURE_DECLS \
	gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_465_proof_Source_GAM_465_Proof_UnlockableDoor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
