// Fill out your copyright notice in the Description page of Project Settings.

#include "MovingPlatform.h"


// Sets default values
AMovingPlatform::AMovingPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PlatformMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Trap Mesh"));
	PlatformMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> PlatformAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> PlatformMaterial(TEXT("/Game/SideScrollerCPP/Materials/M_Metal_Brushed_Nickel"));
	PlatformMesh->SetMaterial(0, PlatformMaterial.Object);

	if (PlatformAsset.Succeeded())
	{
		PlatformMesh->SetStaticMesh(PlatformAsset.Object);
		PlatformMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		PlatformMesh->SetWorldScale3D(FVector(1.25f, 2.25f, 0.25f));
	}

	direction = 1;

}

// Called when the game starts or when spawned
void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMovingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
//Moves the platform between min and max locations set by the player, direction controls the initial direction of movement
	FVector NewLocation = GetActorLocation();

	if (NewLocation.Z > max) {
		
		direction = direction * -1;
	}

	if (NewLocation.Z < min) {

		direction = direction * -1;
	}

	NewLocation.Z += direction * speed;

	SetActorLocation(NewLocation);
}

