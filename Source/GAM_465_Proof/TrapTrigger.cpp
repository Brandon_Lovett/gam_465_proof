// Fill out your copyright notice in the Description page of Project Settings.

#include "TrapTrigger.h"



ATrapTrigger::ATrapTrigger()
{
	GetCollisionComponent()->OnComponentBeginOverlap.AddDynamic(this, &ATrapTrigger::OnComponentBeginOverlap);
	GetCollisionComponent()->OnComponentEndOverlap.AddDynamic(this, &ATrapTrigger::OnComponentEndOverlap);
}

//Will activate the linked spawner(a) from array when player is overlapping
void ATrapTrigger::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	PlayerChara = Cast<AGAM_465_ProofCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if ((OtherActor != nullptr) && (OtherActor == PlayerChara) && (OtherComp != nullptr))
	{
		for (ASpawner* Spawner : LinkedTraps)
		{
			if (Spawner)
			{
				Spawner->FireLoop();
			}
		}
	}
}

//When player leaves the overlap area function will stop the spawners from firing and reset them for if the player comes back
void ATrapTrigger::OnComponentEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	PlayerChara = Cast<AGAM_465_ProofCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if ((OtherActor != nullptr) && (OtherActor == PlayerChara) && (OtherComp != nullptr))
	{
		for (ASpawner* Spawner : LinkedTraps)
		{
			if (Spawner)
			{
				Spawner->StopTimer();
			}
		}
	}
	}