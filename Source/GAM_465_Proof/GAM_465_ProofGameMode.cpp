// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "GAM_465_ProofGameMode.h"
#include "GAM_465_ProofCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGAM_465_ProofGameMode::AGAM_465_ProofGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	HUDClass = AMyHUD::StaticClass();
}
