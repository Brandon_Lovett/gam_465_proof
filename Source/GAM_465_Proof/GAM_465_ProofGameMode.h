// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyHUD.h"
#include "GAM_465_ProofGameMode.generated.h"

UCLASS(minimalapi)
class AGAM_465_ProofGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAM_465_ProofGameMode();
};



