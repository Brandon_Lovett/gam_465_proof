// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorKey.h"


// Sets default values
ADoorKey::ADoorKey()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//Values used for applying rotation to actor
	PitchValue = 0.f;
	YawValue = 0.f;
	RollValue = 0.f;
	
	CapsuleHeight = 40.0f;
	CapsuleRadius = 75.0f;
	//Creates collision component for actor and makes it the root component
	CollisionCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("My Capsule Component"));
	CollisionCapsule->InitCapsuleSize(CapsuleHeight, CapsuleRadius);
	CollisionCapsule->SetCollisionProfileName("Trigger");
	RootComponent = CollisionCapsule;
	//Sets the mesh for the actor and attaches it to the root comonent
	KeyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Key Mesh"));
	KeyMesh->SetupAttachment(RootComponent);

	//Points to the mesh used for the actor
	static ConstructorHelpers::FObjectFinder<UStaticMesh> KeyAsset(TEXT("'/Engine/BasicShapes/Cylinder.Cylinder'"));

	//Points to the material used by the actor and sets it
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> KeyMaterial(TEXT("/Game/SideScrollerCPP/Materials/M_Metal_Gold"));
	KeyMesh->SetMaterial(0, KeyMaterial.Object);

	//Creates the Key if the mesh is found and applied
	if (KeyAsset.Succeeded())
	{
		KeyMesh->SetStaticMesh(KeyAsset.Object);
		KeyMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		KeyMesh->SetWorldScale3D(FVector(0.75f));
	}

	CollisionCapsule->OnComponentBeginOverlap.AddDynamic(this, &ADoorKey::OnOverlapBegin);
	

}

// Called when the game starts or when spawned
void ADoorKey::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADoorKey::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Uses stored values to apply rotation to the key
	FRotator NewRotation = FRotator(PitchValue, YawValue, RollValue);

	FQuat QuatRotation = FQuat(NewRotation);

	AddActorLocalRotation(QuatRotation, false, 0, ETeleportType::None);

}

//When the player overlaps the key will increase the number of keys held by the player and destroy the overlapped key
void  ADoorKey::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	PlayerChara = Cast<AGAM_465_ProofCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if ((OtherActor != nullptr) && (OtherActor == PlayerChara) && (OtherComp != nullptr))
	{
		PlayerChara->PickUpKey();
		Destroy();
	}
}
