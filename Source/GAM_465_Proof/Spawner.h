// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "TrapSpike.h"
#include "Components/SphereComponent.h"
#include "Spawner.generated.h"

UCLASS()
class GAM_465_PROOF_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<AActor> ActorToSpawn;

	UFUNCTION()
		void FireLoop();

	UFUNCTION()
		void ResetTimer();

	UFUNCTION()
		void StopTimer();

	UPROPERTY()
		bool CanFire = true;

	FTimerHandle TrapLoop;

	
	
};
