// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"
#include "Components/PointLightComponent.h"
#include "GAM_465_ProofCharacter.h"
#include "Engine/Engine.h"
#include "Level_End.generated.h"

UCLASS()
class GAM_465_PROOF_API ALevel_End : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevel_End();

protected:

	AGAM_465_ProofCharacter* PlayerChara;

public:

	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* CollisionBox;

	FVector BoxHeight;

	UPROPERTY(EditAnywhere, Category = "LevelName")
		FName LevelName;

	//Opens next Level
	UFUNCTION()
		void LevelEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(VisibleAnywhere, Category = "Light")
		class UPointLightComponent* PointLight;

	// declare light intensity variable
	UPROPERTY(EditAnywhere, Category = "Light Intensity")
		float LightIntensity;

};
