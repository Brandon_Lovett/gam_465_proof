// Fill out your copyright notice in the Description page of Project Settings.

#include "UnlockableDoor.h"


// Sets default values
AUnlockableDoor::AUnlockableDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxHeight = FVector(150.0f, 30.0f, 150.0f);

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("My Box Component"));
	CollisionBox->InitBoxExtent(BoxHeight);
	CollisionBox->SetCollisionProfileName("Trigger");
	RootComponent = CollisionBox;

	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Trap Mesh"));
	DoorMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> DoorAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> DoorMaterial(TEXT("/Game/SideScrollerCPP/Materials/M_Door"));
	DoorMesh->SetMaterial(0, DoorMaterial.Object);

	if (DoorAsset.Succeeded())
	{
		DoorMesh->SetStaticMesh(DoorAsset.Object);
		DoorMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		DoorMesh->SetWorldScale3D(FVector(3.5f, 0.25f, 3.5f));
	}

	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AUnlockableDoor::OnOverlapBegin);

}

// Called when the game starts or when spawned
void AUnlockableDoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUnlockableDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//When a player enters the doors overlap component if they have a key their key count will be reduced by 1 and the door will be destroyed, if they have no key nothing will happen
void  AUnlockableDoor::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	PlayerChara = Cast<AGAM_465_ProofCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if ((OtherActor != nullptr) && (OtherActor == PlayerChara) && (OtherComp != nullptr))
	{
		if (PlayerChara->KeyNum > 0)
		{
			PlayerChara->UseKey();
			Destroy();
		}
		else if (PlayerChara->KeyNum == 0)
		{

		}
	}
}

