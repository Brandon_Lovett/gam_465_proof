// Fill out your copyright notice in the Description page of Project Settings.

#include "Level_End.h"


// Sets default values
ALevel_End::ALevel_End()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxHeight = FVector(150.0f, 100.0f, 150.0f);
	LightIntensity = 3000.0f;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("My Box Component"));
	CollisionBox->InitBoxExtent(BoxHeight);
	CollisionBox->SetCollisionProfileName("Trigger");
	RootComponent = CollisionBox;

	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &ALevel_End::LevelEnd);

	PointLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("Point Light"));
	PointLight->Intensity = LightIntensity;
	PointLight->bVisible = true;
	PointLight->SetLightColor(FLinearColor::Green, true);
	PointLight->SetupAttachment(RootComponent);

}


//When triggered will send the player to the chosen level
void ALevel_End::LevelEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){
	
	PlayerChara = Cast<AGAM_465_ProofCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if ((OtherActor != nullptr) && (OtherActor == PlayerChara) && (OtherComp != nullptr))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Collision Trigger"));
		UGameplayStatics::OpenLevel(this, LevelName);
	}

}

