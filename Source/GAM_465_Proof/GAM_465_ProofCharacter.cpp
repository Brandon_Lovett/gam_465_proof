// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "GAM_465_ProofCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AGAM_465_ProofCharacter::AGAM_465_ProofCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->RelativeRotation = FRotator(0.f,180.f,0.f);

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 3.f;
	GetCharacterMovement()->AirControl = 0.8f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
	
	//Used to track how many key the player has
	KeyNum = 0;
	//used to create the pause menu when players press the pause key
	static ConstructorHelpers::FClassFinder<UUserWidget> PauseMenuWidget(TEXT("/Game/SideScrollerCPP/Menus/PauseMenu"));
	if ((PauseMenuWidget.Class != nullptr))
	{
		PauseMenuWidgetClass = PauseMenuWidget.Class;
	}

}

//////////////////////////////////////////////////////////////////////////
// Input

void AGAM_465_ProofCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGAM_465_ProofCharacter::MoveRight);
	PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &AGAM_465_ProofCharacter::PauseGame);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &AGAM_465_ProofCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AGAM_465_ProofCharacter::TouchStopped);
}

void AGAM_465_ProofCharacter::MoveRight(float Value)
{
	// add movement in that direction
	AddMovementInput(FVector(0.f,-1.f,0.f), Value);
}

void AGAM_465_ProofCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// jump on any touch
	Jump();
}

void AGAM_465_ProofCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	StopJumping();
}
//Restarts the level when the player dies
void AGAM_465_ProofCharacter::PlayerDeath()
{
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}
//Increases the number of keys held by the player when called
void AGAM_465_ProofCharacter::PickUpKey()
{
	KeyNum++;
}
//Decreases the number of keys held by the player when called
void AGAM_465_ProofCharacter::UseKey()
{
	KeyNum--;
}
//Creates the pause menu and applies it to the viewport
void AGAM_465_ProofCharacter::PauseGame()
{
	UUserWidget * PauseMenuRef = CreateWidget<UUserWidget>(GetWorld(), PauseMenuWidgetClass);
	PauseMenuRef->AddToViewport(0);
}
