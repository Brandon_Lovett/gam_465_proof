// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "MyHUD.generated.h"

/**
 * 
 */
UCLASS()
class GAM_465_PROOF_API AMyHUD : public AHUD
{
	GENERATED_BODY()
	
public:

	AMyHUD();

	virtual void BeginPlay() override;


private:

	UPROPERTY(EditAnywhere, Category = "GUI")
		TSubclassOf<class UUserWidget> GUIWidgetClass;
	
	
};
