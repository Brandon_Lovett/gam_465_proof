// Fill out your copyright notice in the Description page of Project Settings.

#include "Spawner.h"


// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();

}

//Resets timer so spawner will fire as long as players are on the trigger
void ASpawner::ResetTimer()
{

		CanFire = true;
		GetWorldTimerManager().ClearTimer(TrapLoop);
		FireLoop();
	
}

//Spawns and launches spike when triggered
void ASpawner::FireLoop()
{
	if (CanFire == true)
	{
		CanFire = false;
		//Variables to get the location and rotation of the spawner
		const FVector Location = GetActorLocation();
		const FRotator Rotation = GetActorRotation();
		//Spawns the selected actor on runtime
		GetWorld()->SpawnActor<AActor>(ActorToSpawn, Location, Rotation);
		GetWorld()->GetTimerManager().SetTimer(TrapLoop, this, &ASpawner::ResetTimer, 2.f, true);
	}

}
//Stops and resets timer so spawner stops firing when player leaves trap trigger
void ASpawner::StopTimer()
{
	GetWorldTimerManager().ClearTimer(TrapLoop);
	CanFire = true;
}
