// Fill out your copyright notice in the Description page of Project Settings.

#include "MyHUD.h"


AMyHUD::AMyHUD()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> GUIWidget(TEXT("/Game/SideScrollerCPP/GUI/Player_GUI"));

	if ((GUIWidget.Class != nullptr))
	{
		GUIWidgetClass = GUIWidget.Class;
	}
}

void AMyHUD::BeginPlay()
{
	UUserWidget * GUIRef = CreateWidget<UUserWidget>(GetWorld(), GUIWidgetClass);

	if (GUIRef) {
		GUIRef->AddToViewport(0);
	}
}