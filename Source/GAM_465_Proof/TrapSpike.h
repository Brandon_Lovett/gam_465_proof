// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"
#include "ConstructorHelpers.h"
#include "GAM_465_ProofCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "TrapSpike.generated.h"

UCLASS()
class GAM_465_PROOF_API ATrapSpike : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATrapSpike();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "SpikeSpeed")
		float speed;


public:	

	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	AGAM_465_ProofCharacter* KillPlayer;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* TrapMesh;

	UPROPERTY(VisibleAnywhere)
		class UCapsuleComponent* MyCollisionCapsule;

	float CapsuleRadius;
	float CapsuleHalfHeight;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void Despawn();

	UPROPERTY(EditAnywhere, Category = "Material")
		UMaterialInterface* TrapMaterial;

	FTimerHandle DespawnTimer;
};
