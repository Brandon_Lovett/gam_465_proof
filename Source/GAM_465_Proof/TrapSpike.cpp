// Fill out your copyright notice in the Description page of Project Settings.

#include "TrapSpike.h"


// Sets default values
ATrapSpike::ATrapSpike()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	//sets basic variables for spike capsule and speed
	CapsuleRadius = 51.f;
	CapsuleHalfHeight = 75.f;
	speed = 300.0f;
	//Creates capsule component and makes it the root component
	MyCollisionCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("My Capsule Component"));
	MyCollisionCapsule->SetCapsuleHalfHeight(CapsuleHalfHeight);
	MyCollisionCapsule->SetCapsuleRadius(CapsuleRadius);
	MyCollisionCapsule->SetCollisionProfileName("Trigger");
	RootComponent = MyCollisionCapsule;

	//Sets the mesh for the actor and attaches it to the root comonent
	TrapMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Trap Mesh"));
	TrapMesh->SetupAttachment(RootComponent);

	//Creates the material for the actor
	TrapMaterial = CreateDefaultSubobject<UMaterialInterface>("Trap Material");

	//Points to the mesh used for the actor
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SpikeAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cone.Cone'"));

	//Points to the material used by the actor and sets it
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> TrapMaterial(TEXT("/Game/SideScrollerCPP/Materials/M_Metal_Steel"));
	TrapMesh->SetMaterial(0, TrapMaterial.Object);

	//Creates the Trap Spike if the mesh is found and applied
	if (SpikeAsset.Succeeded())
	{
		TrapMesh->SetStaticMesh(SpikeAsset.Object);
		TrapMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 25.0f));
		TrapMesh->SetWorldScale3D(FVector(1.f));
	}

	MyCollisionCapsule->OnComponentBeginOverlap.AddDynamic(this, &ATrapSpike::OnOverlapBegin);

}

// Called when the game starts or when spawned
void ATrapSpike::BeginPlay()
{
	Super::BeginPlay();

	//Timer for deleting spikes after they spawn
	GetWorld()->GetTimerManager().SetTimer(DespawnTimer, this, &ATrapSpike::Despawn, 3.f, true, 3.f);
	
}

// Called every frame
void ATrapSpike::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	// Moves spike in its relative forward direction (towards tip)
	FVector location = GetActorLocation();
	location += GetActorUpVector() * speed * DeltaTime;
	SetActorLocation(location);

}

void ATrapSpike::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//Function calls to player character and activates death function if the player touches the spike
	KillPlayer = Cast<AGAM_465_ProofCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if ((OtherActor != nullptr) && (OtherActor == KillPlayer) && (OtherComp != nullptr))
	{
		KillPlayer->PlayerDeath();
	}
}

//Destroys the spike when called
void ATrapSpike::Despawn()
{
	Destroy();
}
