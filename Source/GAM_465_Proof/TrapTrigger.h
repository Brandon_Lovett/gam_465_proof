// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Components/ShapeComponent.h"
#include "Spawner.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "GAM_465_ProofCharacter.h"
#include "Components/PointLightComponent.h"
#include "TrapTrigger.generated.h"

/**
 * 
 */
UCLASS()
class GAM_465_PROOF_API ATrapTrigger : public ATriggerBox
{
	GENERATED_BODY()

public:
	ATrapTrigger();

protected:

	UFUNCTION()
		void OnComponentBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnComponentEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY(EditAnywhere)
		TArray<ASpawner*> LinkedTraps;

	AGAM_465_ProofCharacter* PlayerChara;

};
