// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"
#include "MovingPlatform.generated.h"

UCLASS()
class GAM_465_PROOF_API AMovingPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMovingPlatform();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* PlatformMesh;

	UPROPERTY(EditAnywhere, Category = "ActorDirection")
		int direction;

	UPROPERTY(EditAnywhere, Category = "ActorMaxHeight")
		float max;

	UPROPERTY(EditAnywhere, Category = "ActorMinHeight")
		float min;

	UPROPERTY(EditAnywhere, Category = "ActorSpeed")
		float speed;


	
	
};
